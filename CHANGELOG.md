- **1.0.6** (2021-03-11):
    - fixed pseudonymisation of
      integer identifiers

- **1.0.5** (2021-03-11):
    - fixed the command line tool to allow
      downloading from all supported country
      versions of OLX

- **1.0.4** (2021-01-11):
    - fixed typo in setup.cfg

- **1.0.3** (2021-01-11):
    - identifiers now pseudonymised
    - build system changed to pep517
    - migrated cli utils to sqlalchemy from psycopg2

- **1.0.2** (2020-12-04):
    - removed Arch Linux PKGBUILD

- **1.0.1** (2020-09-29):
    - fixed bug in olxsearchbasea (KeyError: 'data')

- **1.0.0** (2020-09-07):
    - PKGBUILD for Arch Linux (user repository)
    - miscellaneous bug fixes

- **0.9.0** (2020-06-24):
    - first feature-complete version

- **0.0.1** (2019-03-26):
    - first test version
